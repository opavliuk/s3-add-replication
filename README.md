# AWS S3 Add Replication Script

## About

**AWS S3 Add Replication by AWS CLI**

**Script:** [`s3-add-replication.sh`](s3-add-replication.sh)  
**AWS CLI:** `version 2`  
**Language:** `Bash`

This is script gives the automated process of 
["AWS Docs - Configuring replication when the source and destination buckets are owned by the same account"](https://docs.aws.amazon.com/AmazonS3/latest/dev/replication-walkthrough1.html).

**Author:** `Oleksii Pavliuk` __(pavliuk.aleksey@gmail.com)__  
**Date:** `06/30/2020`

`[!]` The script wrote on the Mac OS system and can have undefined behavior
      on another operating system.

## Features
There are some scripts features which you need to know!

The script does:
-  offering to create this, if:
    - buckets is absent;
    - versioning of buckets is disabled; 
    - a unique replication role for a replication rule is absent;
- automated creating a config file and attach the policy to
  the replication role if it is absent.
- automated creating these configuration files if need:
    - **./s3-role-trust-policy.json** (for request to create role);
    - **./s3-role-policy-$SRC_BUCKET.json** (for request to attach policy to role);
    - **./s3-replication-$SRC_BUCKET.json** (for request to add replication rule to bucket);
- synchronizing buckets and coping all objects to destination bucket before
  adding a replication rule;
- checking buckets size after synchronization;
- ALWAYS asking to confirm synchronizing buckets and adding a replication rule,
  it means flag `--without-approve` doesn't turn off this.


## Before using the script

Before using this script:
- sure that you have full-access permissions to **AWS IAM** and **AWS S3**;
- [install](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) and [configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) **AWS CLI version 2**.

## Templates

Directory [templates](templates) consists configuration files which needs for requests in **AWS**:
- **[s3-role-trust-policy.json.tenplate](templates/s3-role-trust-policy.json.template)**
    (for request to create role in AWS IAM);
- **[s3-role-policy-{bucket}.json.template](templates/s3-role-policy-{bucket}.json.template)**
    (for request to attach policy to role in AWS IAM);
- **[s3-replication-{bucket}.json.template](templates/s3-replication-{bucket}.json.template)**
    (for request to add replication rule to bucket AWS S3);

`[!]` One of the script features: filling these files isn't required,
      script do it by itself. 

## Usage
```shell script
 Usage: ./s3-add-replication.sh [OPTIONS]  
    OPTIONS:
       [required]:
          --src-bucket ARG              : a name of source bucket, which we want to replicate  
          --dest-bucket ARG             : a name of destination bucket, it is replica  
       [optional]:
          --aws-region ARG              : AWS Region [default: ] 
          --cli-profile ARG             : profile in AWS CLI [default: default]
          --log-file-path ARG           : path to log file for logging [default: ./s3-add-replication.log]  
          --without-approve             : do all processes without user approve:  
                                            * create buckets;
                                            * enable versioning;  
                                            * create role;
                                            * attach policy to role.  
        -h | --help                   : show this usage
 
Example: ./s3-add-replication.sh --src-bucket SRC_BUCKET --dest-bucket DEST_BUCKET  
```
